# Python 메모리 관리: 가비지 컬렉션 <sup>[1](#footnote_1)</sup>

<font size="3">Python이 참조 계수와 가비지 컬렉션 기법을 사용하여 메모리를 관리하는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./memory-management.md#intro)
1. [Python이 메모리를 할당하는 방법](./memory-management.md#sec_02)
1. [참조 계수란 무엇이며 그 작동 방식](./memory-management.md#sec_03)
1. [가비지 컬렉션이란 무엇이며 그 작동 방식](./memory-management.md#sec_04)
1. [Python에서 메모리 사용량을 모니터링하고 제어하는 방법](./memory-management.md#sec_05)
1. [일반적인 메모리 관리 오류와 이를 방지하는 방법](./memory-management.md#sec_06)
1. [요약](./memory-management.md#summary)

<a name="footnote_1">1</a>: [Python Tutorial 33 — Python Memory Management: Garbage Collection](https://python.plainenglish.io/python-tutorial-33-python-memory-management-garbage-collection-93ecf4a99162)를 편역한 것이다.
